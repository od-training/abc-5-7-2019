import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap, filter } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { Video } from 'src/app/app-types';
import { VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, videoSvc: VideoDataService) {
    this.video = route.queryParams.pipe(
      filter(params => params['videoId']),
      map(params => params['videoId']),
      switchMap(id => videoSvc.loadSingleVideo(id))
    );
  }

  ngOnInit() {
  }

}
