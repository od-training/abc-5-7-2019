import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from './app-types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos').pipe(
        map(videos => [
          ...videos,
          {
            title: 'Added from map!',
            author: 'map',
            id: 'aa',
            viewDetails: []
          }
        ])
      );
  }

  loadSingleVideo(id: string): Observable<Video> {
    return this.http.get<Video>('https://api.angularbootcamp.com/videos/' + id);
  }
}
